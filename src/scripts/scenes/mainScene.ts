export default class MainScene extends Phaser.Scene {
  fpsText

  constructor() {
    super({ key: 'MainScene' })
  }

  create() {
    
    const map = this.make.tilemap({ key: 'tilemap'})
    const tileset = map.addTilesetImage('jumpcat', 'tiles')

    const platFormsLayer = map.createLayer('Platforms', tileset)
    platFormsLayer.setCollisionByExclusion([-1], true)

    const player = this.physics.add.sprite(240, 650, 'main');
    //player.setCollideWorldBounds(true);
    this.physics.add.collider(player, platFormsLayer);

    this.cameras.main.scrollY = 480
  }

  update() {

  }
}
